package main

import (
	"log"

	"github.com/EdisonPeterson/twittor/bd"
	"github.com/EdisonPeterson/twittor/handlers"
)

func main() {
	if bd.ChequeoConnection() == 0 {
		log.Fatal("Sin conexión a la BD")
		return
	}
	handlers.Manejadores()
}
